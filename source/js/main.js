let arrow = document.querySelector(".arrow");

dragElement(arrow);

function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    // move the DIV from anywhere inside the DIV:
    elmnt.onmousedown = dragMouseDown;

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    // stop moving when mouse button is released:
    document.onmouseup = null;
    document.onmousemove = null;
  }
}

function setDegree(staticDegree) {
  arrow.style.cssText += "transform: rotate(" +staticDegree+ "deg);";
}

function setOrigin(x,y) {
  arrow.style.cssText += "transform-origin: " + x + "px " + y + "px;";
}

arrow.ondblclick = function(e) {
  let centerX = e.offsetX;
  let centerY = e.offsetY;
  var x = e.offsetX==undefined?e.layerX:centerX;
  var y = e.offsetY==undefined?e.layerY:centerY;
  setOrigin(x,y);
  console.log(x +' x '+ y);
}

function setRotate(from, to, speed, acceleration) {
  let style = document.createElement('style');
  style.type = 'text/css';
  style.id = 'animation';
  let css = "@keyframes rotate { from { transform: rotate(" + from + "deg);} to { transform: rotate(" + to + "deg);}}";
  css += " .arrow--rotate {animation-duration: " + speed + "s; animation-timing-function: " + acceleration + ";}";
  style.appendChild(document.createTextNode(css));
  document.head.appendChild(style);
};

function play() {
  arrow.classList.add('arrow--rotate');
}

function reset() {
  arrow.classList.remove('arrow--rotate');
  let styleSheet = document.querySelector("#animation");
  document.head.removeChild(styleSheet);
}
